---
title: Let's Encrypt on Turris Omnia
date: 2019-05-18
lastmod: 2023-01-18
tags:
- Let's Encrypt
- Turris Omnia
---

This is a guide about how I use [Let's Encrypt][letsencrypt] certificates on my [Turris Omnia][turris_omnia] router.

<!--more-->

This guide assumes that you already have a domain pointing to your router and that it's IP is updated when the IP of your router changes.

[letsencrypt]: https://letsencrypt.org/
[turris_omnia]: https://www.turris.cz/en/turris-omnia/

# Connect to the router

For any of these steps, you need to ssh to your router:

```bash
ssh root@your.domain
```

# Install acme.sh

The following command will install [`acme.sh`][acme_sh] (a [Let's Encrypt][letsencrypt] client) in `/root/.acme.sh/`.
The install script automatically creates a cronjob for it.
Adjust the email address to receive emails should a certificate expire.

```bash
EMAIL=email@your.domain
cd /root
curl https://get.acme.sh | sh
./.acme.sh/acme.sh --accountemail "$EMAIL" --register-account # First Time Only
./.acme.sh/acme.sh --accountemail "$EMAIL" --update-account
```

[acme_sh]: https://acme.sh

# Configure Lighttpd

We're going to use `acme.sh` in webroot mode.
Therefore we must slightly extend lighttpd's config.

```bash
echo 'alias.url += ( "/.well-known/acme-challenge/" => "/www/letsencrypt/.well-known/acme-challenge/")' \
     > /etc/lighttpd/conf.d/80-letsencrypt.conf
mkdir -p /www/letsencrypt/.well-known/acme-challenge/
```

# Configure the Firewall

This adds a rule which is disabled by default.
When enabled, the rule allows traffic to port 80 on your router.
Every time a certificate is renewed, this rule will be temporarily enabled.
It will automatically get disabled after the certificate was renewed.

```bash
uci set firewall.letsencrypt=rule
uci set firewall.letsencrypt.target=ACCEPT
uci set firewall.letsencrypt.src=wan
uci set firewall.letsencrypt.proto=tcp
uci set firewall.letsencrypt.dest_port=80
uci set firewall.letsencrypt.name='allow http on wan'
uci set firewall.letsencrypt.enabled=0
uci commit firewall
/etc/init.d/firewall reload
```

Note: Make sure, that traffic from `wan` zone to the `input` chain is allowed!

# Issue the First Certificate

> See below if you enabled Minipot on your Router

```bash
DOMAIN=your.domain
/root/.acme.sh/acme.sh \
    --issue \
    -d "$DOMAIN" \
    -w /www/letsencrypt/ \
    --pre-hook "uci set firewall.letsencrypt.enabled=1; uci commit firewall; /etc/init.d/firewall reload" \
    --post-hook "uci set firewall.letsencrypt.enabled=0; uci commit firewall; /etc/init.d/firewall reload" \
    --reloadcmd "cat /root/.acme.sh/$DOMAIN/$DOMAIN.cer /root/.acme.sh/$DOMAIN/$DOMAIN.key > /etc/lighttpd-self-signed.pem; /etc/init.d/lighttpd restart"
```

`acme.sh` will automatically renew the certificate after it's issued for the first time.

## If Minipot is Active

If you run [a Minipot honeypot][minipot] on your router,
then you will see a conflict on port 80.
The solution is to disable Minipot while updating the certificate.
Use the following command to get your certificate instead:

```bash
DOMAIN=your.domain
/root/.acme.sh/acme.sh \
    --issue \
    -d "$DOMAIN" \
    -w /www/letsencrypt/ \
    --pre-hook "uci set sentinel.minipot.http_port=0; uci set firewall.letsencrypt.enabled=1; uci commit firewall; /etc/init.d/firewall reload" \
    --post-hook "uci delete sentinel.minipot.http_port; uci set firewall.letsencrypt.enabled=0; uci commit firewall; /etc/init.d/firewall reload" \
    --reloadcmd "cat /root/.acme.sh/$DOMAIN/$DOMAIN.cer /root/.acme.sh/$DOMAIN/$DOMAIN.key > /etc/lighttpd-self-signed.pem; /etc/init.d/lighttpd restart"
```

# Updates

* 2023-01-18: Sebastian suggested to use `80-letsencrypt.conf` instead of just `letsencrypt.conf`. Thanks!
* 2020-05-20: [lucasrangit mentioned in the Turris forum][turris-forum] that `--register-account` needs to be run before `--update-account` can be run. Thanks!
* 2020-12-29: [Andrzej from Cabal Software](https://cabal.pl) provided a solution that works when [Minipots][minipot] is active. Thanks!

[turris-forum]: https://forum.turris.cz/t/guide-adding-a-lets-encrypt-certificate-to-the-turris-omnia/10191/6
[minipot]: https://gitlab.nic.cz/turris/sentinel/minipot
