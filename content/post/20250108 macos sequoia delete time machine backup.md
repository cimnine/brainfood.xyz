---
title: Delete Time Machine backups in macOS Sequoia
date: 2025-01-08
tags:
- macos
- timemachine
---

This short guide explains how to use the `tmutil` to delete a single _Time Machine_ backup.

<!--more-->

This might be necessary if you have a single file in your backup that shouldn't be backed up.
For example, while migrating your password manager,
you might have exported your passwords as JSON or CSV to the Desktop.
And that file then ended up in a _Time Machine_ backup.
That's undesired.

> Side note: You do have a password manager, right?!

The following guide works with macOS Sequoia.

## Preparation

Open your _System Settings_.
Go to _Privacy & Security_.
Go to _Full Disk Access_.
Find your terminal application and allow _Full Disk Access_ for it.
I'm using _iTerm_, but _Terminal_ works just as well.

## Find the last non-affected backup

Since Apple changed _Time Machine_ to APFS,
it's no longer possible to delete a single file from the backup.
So you need to know how many complete _Time Machine_ backups you need to delete.
This is usually the day before the day on which the compromising file was first saved to disk
and thereafter got added to the backup.

You can confirm this by opening the _Time Machine_ application.
Navigate to the compromised folder.
Then scroll through the backups until you find a backup that is no longer compromised.
Note down the date and time of the backup.

## Delete Local Snapshots

Open your terminal application, then type (or paste) the following command.
It will return a list of all snapshots currently kept.
Your result should look similar to mine.

```shell
$ tmutil listlocalsnapshots /
Snapshots for disk /:
com.apple.TimeMachine.2025-01-07-211937.local
com.apple.TimeMachine.2025-01-07-221601.local
com.apple.TimeMachine.2025-01-08-001432.local
com.apple.TimeMachine.2025-01-08-011432.local
com.apple.TimeMachine.2025-01-08-021702.local
com.apple.TimeMachine.2025-01-08-042344.local
com.apple.TimeMachine.2025-01-08-052417.local
…
```

Next, you need to delete all the affected snapshots.

```shell
$ sudo tmutil deletelocalsnapshots 2025-01-07-221601
Deleted local snapshot '2025-01-07-221601'
```

> Tipp: I copied the output to my favorite text editor and
> used [multi-line selection][vscode-basics] to put a cursor on each line.
> Now, I made sure that every line reads `sudo tmutil deletelocalsnapshots [date]`.
> Then, I copied the output back to my terminal and hit `Enter`.

[vscode-basics]: https://code.visualstudio.com/docs/editor/codebasics#_multiple-selections-multicursor

## Delete all affected Backups

Back in your terminal application, type (or paste) the following command.
It will return a list of all the dates for which _Time Machine_ backups currently exist.
Your result should look similar to mine.

```shell
$ tmutil listbackups -t
2025-01-01-125101
2025-01-02-123459
2025-01-04-103100
2025-01-05-102617
2025-01-06-084051
2025-01-07-183749
2025-01-07-222240
2025-01-08-001712
2025-01-08-011526
2025-01-08-030734
2025-01-08-050120
…
```

In the next step, you need to delete all the affected backups.
For this, you need to know the mount path of your _Time Machine_.
So, if your _Time Machine_ disk is called `backup`,
then the mount path will be `/Volumes/backup`.

<!-- markdownlint-disable-next-line MD038 -->
> If in doubt, you can also type `echo ` (with a trailing space!) into your terminal application.
> Then drag-and-drop your _Time Machine_ from the _Desktop_ (or another _Finder_ window) into the terminal application.
> Hit enter, and it will tell you the mount path of your _Time Machine_ disk.
>
> Alternatively, open the _Disk Utility_ application.
> Select your _Time Machine_ disk on the left.
> You will see the mount path under _Mount Point_.

Once you know the mount path of your _Time Machine_ disk,
you can begin the process of deleting all affected backups.
For this, use the following command:

```shell
$ sudo tmutil delete -d /Volumes/backup -t 2025-01-01-125101
1 backups deleted
```

The previous command assumed that the mount path for the _Time Machine_ disk is `/Volumens/backup`.
It also deleted the backup of `2025-01-01`, taken at `12:51:01`.

## Conclusion

Don't put compromising files in places where they are backed up.

Thanks to [_massisenergy_ on the _AskDifferent StackExchange_][a-se] for pointing out
that the actual mount path of the _Time Machine_ disk was expected by `tmutil delete` instead of `/Volumens/.timemachine`.

[a-se]: https://apple.stackexchange.com/a/471920
