---
title: About me
comments: false
---

I'm Chris. My online handle is usually "[cimnine][cimnine.ch]". More details about me on my website [cmader.ch][cmader.ch].

[cimnine.ch]: https://cimnine.ch
[cmader.ch]: https://cmader.ch