[![Pipeline Status](https://gitlab.com/cimnine/brainfood.xyz/badges/master/pipeline.svg)](https://gitlab.com/cimnine/brainfood.xyz/commits/master)
[![Netlify Status](https://api.netlify.com/api/v1/badges/8b9a2b63-2121-4c5b-8630-5ffdbb5864cf/deploy-status)](https://app.netlify.com/sites/brainfoodxyz/deploys)

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Hugo
1. Preview your project: `hugo server`
1. Add content
1. Generate the website: `hugo` (optional)

Read more at Hugo's [documentation][].

### Preview your site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/hugo/`.

The theme used is adapted from http://themes.gohugo.io/beautifulhugo/.

## Themes

Themes are added via subtree, so that they are available right away when cloning.

### Add Themes

```bash
git subtree add --prefix themes/beautifulhugo \
    --squash https://github.com/halogenica/beautifulhugo.git master
```

### Update Themes

```bash
git subtree pull --prefix themes/beautifulhugo \
	--squash https://github.com/halogenica/beautifulhugo.git master
```
