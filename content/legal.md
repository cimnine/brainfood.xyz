---
title: Legal
comments: false
---

# Sharing is Caring

Except when stated otherwise, any work on this website is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

# Privacy

This site is hosted by Cloudflare and served through their CDN.
Their analytics tool is used to collect some basic anonymous stats about this site.
See their [Privacy Policy][cloudflare] for all the data they're collecting.

[cloudflare]: https://www.cloudflare.com/privacypolicy/

# Cookies

Cloudflare sets a cookie named `__cfduid`.
This cookie is used by Cloudflare to distinguish users who use a shared IP address in order to verify requests
and it is technically required.
