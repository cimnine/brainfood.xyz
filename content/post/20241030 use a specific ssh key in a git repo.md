---
title: Use a specific SSH key for a certain git repo
date: 2024-10-30
tags:
- GitLab
- git
- ssh
---

Whenever I want to push code to my private GitLab repositories,
I want to use the SSH key of my private GitLab account.
At work, I use a separate account for GitLab.
It has its own SSH key.

<!--more-->

## Concept

We're going to use a clever combination of _OpenSSH_ configuration and _git_ configuration.

In _OpenSSH_, we'll create a virtual host.
The virtual host will be `private.gitlab.com`.
We'll simply tell _OpenSSH_ that whenever it sees `private.gitlab.com`,
it must connect to `gitlab.com` instead.

In _git_, we'll create a virtual URL.
We'll tell _git_ for one specific repository,
that whenever it sees `git@gitlab.com` it shall use `git@private.gitlab.com` instead.

_OpenSSH_ will then translate that back to `gitlab.com`.
But it will use the correct SSH key in the process.

> By the way, this concept also works with other _git_ foundries, such as GitHub.

## Assumptions

You have two ssh keys:

- `~/.ssh/id_ed25519_work@gitlab.com` is your work ssh key.
- `~/.ssh/id_ed25519_private@gitlab.com` is you private ssh key.

## OpenSSH config

First, edit `${HOME}/.ssh/config`.

```openssh
# These three lines are optional, but recommended
AddKeysToAgent yes
UpdateHostKeys yes
HashKnownHosts yes

Host gitlab.com
  IdentitiesOnly yes
  # Main gitlab.com SSH key
  IdentityFile ~/.ssh/id_ed25519_work@gitlab.com

Host private.gitlab.com
  IdentitiesOnly yes
  # Translate private.gitlab.com back to gitlab.com
  Hostname gitlab.com
  # Other gitlab.com SSH key
  IdentityFile ~/.ssh/id_ed25519_private@gitlab.com
```

## _git_ config

In that private _git_ repository, configure the following.

```shell
git config url.git@private.gitlab.com:.insteadOf git@gitlab.com:
```

> **Hint:**
> You can also change your committer name and/or email only for one repository the same way:
>
> ```shell
> git config user.name 'l33t h4x0r'
> git config user.email l33t@h4x0r.email
> ```
